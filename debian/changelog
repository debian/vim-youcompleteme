vim-youcompleteme (0+20240827+gitb6e8c64+ds-3) unstable; urgency=medium

  * Import upstream 0+20241009+git35d1882 as patch series
  * Denote upstream vim requirements in Depends
  * Drop old versioned (build-)requirements on ycmd

 -- David Kalnischkies <donkult@debian.org>  Tue, 22 Oct 2024 14:10:17 +0200

vim-youcompleteme (0+20240827+gitb6e8c64+ds-2) unstable; urgency=medium

  * Fix hierarchies test (order difference) on s390x

 -- David Kalnischkies <donkult@debian.org>  Sat, 14 Sep 2024 17:09:24 +0200

vim-youcompleteme (0+20240827+gitb6e8c64+ds-1) unstable; urgency=medium

  [ David Kalnischkies ]
  * New upstream version 0+20240827+gitb6e8c64+ds
    - Adds support for type and call hierarchies
    - Refresh patches
  * Bump upstream copyright year to 2024
  * Bump Standards-Version to 4.7.0 (no change needed)

  [ Alexandre Detiste ]
  * remove python3-mock build-dep, package uses "unittest.mock" now

 -- David Kalnischkies <donkult@debian.org>  Tue, 10 Sep 2024 21:57:38 +0200

vim-youcompleteme (0+20231230+git71166ea+ds-2) unstable; urgency=medium

  * Import upstream 0+20240114+git2b33bf3 as patch series
  * Drop extraneous obsolete build-dependency on python3-future, too

 -- David Kalnischkies <donkult@debian.org>  Mon, 15 Jan 2024 15:06:22 +0100

vim-youcompleteme (0+20231230+git71166ea+ds-1) unstable; urgency=medium

  [ David Kalnischkies ]
  * New upstream version 0+20231230+git71166ea+ds
    - Refresh patches and drop backported ones
  * Bump my copyright year as advised by lintian
  * Patch to support different ycmd versions in tests

  [ Alexandre Detiste ]
  * Remove extraneous obsolete dependency on python3-future

 -- David Kalnischkies <donkult@debian.org>  Wed, 03 Jan 2024 00:59:22 +0100

vim-youcompleteme (0+20230612+git49ced5a+ds-3) unstable; urgency=medium

  [ David Kalnischkies ]
  * Import upstream 0+20231006+gitcc9a3ae as patch series
  * Fix completer typo in Debian specific config :help entries
  * Patch to support different clangd versions in tests
  * Allow building source after successful build (Closes: #1046127)

  [ Josh Cummings ]
  * Fix completion typo in README.Debian (Closes: #1054996)

 -- David Kalnischkies <donkult@debian.org>  Sun, 05 Nov 2023 12:37:30 +0100

vim-youcompleteme (0+20230612+git49ced5a+ds-2) unstable; urgency=medium

  * Import upstream 0+20230819+git4f1dcf4 as patch series
    - Accept command modifiers for GetDoc subcommand
    - Add option to disable signature help syntax highlighting

 -- David Kalnischkies <donkult@debian.org>  Mon, 28 Aug 2023 18:53:59 +0200

vim-youcompleteme (0+20230612+git49ced5a+ds-1) unstable; urgency=medium

  * New upstream version 0+20230612+git49ced5a+ds
    - Performance improvements for semantic highlighting
    - Refresh patches

 -- David Kalnischkies <donkult@debian.org>  Wed, 21 Jun 2023 16:37:30 +0200

vim-youcompleteme (0+20230109+git7620d87+ds-3) unstable; urgency=medium

  * Import upstream 0+20230207+git9a5eb44 as patch series
    - Fix traceback when diagnostics contain quotes

 -- David Kalnischkies <donkult@debian.org>  Thu, 09 Feb 2023 16:02:29 +0100

vim-youcompleteme (0+20230109+git7620d87+ds-2) unstable; urgency=medium

  * Import upstream 0+20230117+git50379d3 as patch series
    - Hide diagnostics UI when entering Insert Mode
    - Don't change cursor position if GoTo doesn't have one
  * Bump Standards-Version to 4.6.2 (no changes needed)

 -- David Kalnischkies <donkult@debian.org>  Wed, 01 Feb 2023 00:14:46 +0100

vim-youcompleteme (0+20230109+git7620d87+ds-1) unstable; urgency=medium

  * New upstream version 0+20230109+git7620d87+ds
    - Support C-x C-u to manually trigger YCM completer
    - README and vim documentation are sync'ed better
  * Add Repository field in upstream/metadata

 -- David Kalnischkies <donkult@debian.org>  Sat, 14 Jan 2023 20:51:53 +0100

vim-youcompleteme (0+20220824+git7c3a88f+ds-3) unstable; urgency=medium

  * Fix registry file for deprecated vim-addon-manager (Closes: #1019188)

 -- David Kalnischkies <donkult@debian.org>  Tue, 06 Sep 2022 14:06:38 +0200

vim-youcompleteme (0+20220824+git7c3a88f+ds-2) unstable; urgency=medium

  * Import upstream 0+20220829+gitd4343e8 as patch series
    - Fix passing '+10' etc on command line in older vims
    - Fixes traceback when a long identifier is displayed in finder
  * Double WaitFor* timeouts in autopkgtest for armel

 -- David Kalnischkies <donkult@debian.org>  Sat, 03 Sep 2022 09:11:01 +0200

vim-youcompleteme (0+20220824+git7c3a88f+ds-1) unstable; urgency=medium

  * New upstream version 0+20220824+git7c3a88f+ds
    - Adds highly experimental inlet hints support

 -- David Kalnischkies <donkult@debian.org>  Fri, 26 Aug 2022 18:28:26 +0200

vim-youcompleteme (0+20220402+git3ededae+ds-4) unstable; urgency=medium

  * Import upstream 0+20220706+gitd35df61 as patch series
    - Support ycmds experimental semantic highlighting
  * Do not require semantic highlighting support in ycmd
  * Run gopls test with gccgo and golang-go
  * Use first option in gopls fixit test dialog (Closes: #1016842)
  * Avoid testing with unavailable nodejs on armel

 -- David Kalnischkies <donkult@debian.org>  Mon, 08 Aug 2022 21:55:47 +0200

vim-youcompleteme (0+20220402+git3ededae+ds-3) unstable; urgency=medium

  * Ignore arch-specific details in hover test fixing autopkgtest on i386

 -- David Kalnischkies <donkult@debian.org>  Sun, 12 Jun 2022 13:33:59 +0200

vim-youcompleteme (0+20220402+git3ededae+ds-2) unstable; urgency=medium

  * Reflect Debian's ycmd LSP config change in documentation patch
  * autopkgtest related changes:
    - Split autopkgtest vim-tests into multiple tests
    - Retry failed vim tests 10 times in autopkgtest
    - Skip autopkgtest with ccls if it isn't installable
    - Add a simple test for fortran completer usage

 -- David Kalnischkies <donkult@debian.org>  Sat, 11 Jun 2022 15:52:54 +0200

vim-youcompleteme (0+20220402+git3ededae+ds-1) unstable; urgency=medium

  [ David Kalnischkies ]
  * New upstream version 0+20220402+git3ededae+ds
    - Drop old hamcrest api patch as it is available in Debian now
    - Update patches and drop upstream backports
    - Remove python-requests and pytest as unneeded now
  * Use dh-vim-addon (Closes: #769812)
  * Don't use markdown syntax in doc file for vim (Closes: #1011623)
  * Add neovim as possible alternative client (Closes: #863619)
  * Use upstream vim tests as autopkgtest
  * Add smoke tests for libclang, ccls and gopls usage
  * Bump Standards-Version to 4.6.1 (no change needed)

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

 -- David Kalnischkies <donkult@debian.org>  Thu, 09 Jun 2022 23:32:12 +0200

vim-youcompleteme (0+20200825+git2afee9d+ds-2) unstable; urgency=medium

  * Import new upstream 0+20201025+gitdd61ad7 as patch series
    - Lower priority of diagnostic highlights to fix hlsearch
    - Use more asynchrony for hover and completions requests
  * Refresh and update patches

 -- David Kalnischkies <donkult@debian.org>  Wed, 28 Oct 2020 15:20:45 +0100

vim-youcompleteme (0+20200825+git2afee9d+ds-1) unstable; urgency=medium

  * New upstream version 0+20200825+git2afee9d+ds
    - some small fixes for auto-hover popup
    - drops python3 <= 3.5 support,
      but even debian/stable has >= 3.7 already
  * Refresh local and drop included upstream patches
  * Bump debhelper-compat from 12 to 13

 -- David Kalnischkies <donkult@debian.org>  Mon, 31 Aug 2020 14:33:42 +0200

vim-youcompleteme (0+20200314+git3108b9b+ds-4) unstable; urgency=medium

  * Import upstream 0+20200505+git630db01+ds as patch series
    - Implement FileSave event
    - Add Auto-hover popup g:ycm_auto_hover (enabled by default)

 -- David Kalnischkies <donkult@debian.org>  Wed, 06 May 2020 08:59:46 +0200

vim-youcompleteme (0+20200314+git3108b9b+ds-3) unstable; urgency=medium

  * Import upstream 0+20200418+git367c151+ds as patch series
    - When available, use complete() and TextChangedP rather than completefunc
      to reduce flicker and noticeable redraw latency on recent vims
    - Add range and mods to YcmToggleLogs, e.g. :vertical 90YcmToggleLogs

 -- David Kalnischkies <donkult@debian.org>  Tue, 28 Apr 2020 11:01:13 +0200

vim-youcompleteme (0+20200314+git3108b9b+ds-2) unstable; urgency=medium

  * Import upstream 0-20200406-git97150c0-ds as patch
  * Use default settings from ycmd directly instead of embedding them

 -- David Kalnischkies <donkult@debian.org>  Fri, 10 Apr 2020 15:21:25 +0200

vim-youcompleteme (0+20200314+git3108b9b+ds-1) unstable; urgency=medium

  [ Jack Bates ]
  * Vim changed Provides vim-python to vim-python3

  [ David Kalnischkies ]
  * New upstream version 0+20200314+git3108b9b+ds
  * Bump year in Copyright stanzas in d/copyright
  * Bump Standards-Version to 4.5.0 (no changes needed)
  * Refresh patches removing third_party import attempts
  * Build-dep on pytest instead of nose for testing
  * Add patch to use old hamcrest API in tests

 -- David Kalnischkies <donkult@debian.org>  Sun, 15 Mar 2020 21:36:26 +0100

vim-youcompleteme (0+20191218+git9e2ab00+ds-1) unstable; urgency=medium

  * Set myself as maintainer via one-year hijack (see #912690)
  * New upstream version 0+20191218+git9e2ab00
    - Advertise debian repack from git HEAD
    - Update d/copyright for 2019
    - Update patches
  * Bump Standards-Version (no change required)
  * Bump debhelper compat to 12
  * Use ycm-core namespace instead of valloric as upstream
  * Build-Depend on recent ycmd for core version 42
  * Remove pointless ancient x-python-version >= 3.5

 -- David Kalnischkies <donkult@debian.org>  Sun, 29 Dec 2019 16:40:19 +0100

vim-youcompleteme (0+20190211+gitcbaf813-0.1) unstable; urgency=medium

  * Non-maintainer upload.
  * New upstream version 0+20190211+gitcbaf813
    - Refresh patches
    - Drop (Build-)Depends on python3-frozendict
    - Update sign place regex pattern for vim >= 8.1.0614
  * Bump Standards-Version to 4.3.0 (no changes)

 -- David Kalnischkies <donkult@debian.org>  Thu, 14 Feb 2019 11:38:37 +0100

vim-youcompleteme (0+20181101+gitfaa019a-0.1) unstable; urgency=medium

  * Non-maintainer upload.

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Set Vcs-* to salsa.debian.org

  [ Sylvestre Ledru ]
  * New upstream release
    - Refresh path-to-server-script.patch

  [ David Kalnischkies ]
  * New upstream version 0+20181101+gitfaa019a
    - Use uscan to generate tarball from upstream git HEAD
    - Refresh path-to-server-script.patch
  * Depend on ycmd via ycmd-core-version provides (Closes: #912030)
  * Don't use dpkg-parsechangelog directly in debian/rules
  * Switch from debhelper 9 to 11
  * Bump Standards-Version to 4.2.1 (no changes)
  * Run nose-based tests at buildtime
  * Use https for upstream homepage
  * Set R³: no in debian/control

 -- David Kalnischkies <donkult@debian.org>  Sat, 03 Nov 2018 01:35:40 +0100

vim-youcompleteme (0+20161219+git194ff33-1) unstable; urgency=low

  * New upstream release. (Closes: #850013, #850113)
  * Refresh patches for new upstream release.
  * Replace ycmd dependency with latest version.

 -- Onur Aslan <onur@onur.im>  Thu, 26 Jan 2017 12:57:50 +0300

vim-youcompleteme (0+20160327+git1b76af4-2) unstable; urgency=low

  * Add python3 support. (Closes: #837646)
  * Bump Standards-Version to 3.9.8 (no changes).

 -- Onur Aslan <onur@onur.im>  Thu, 15 Sep 2016 12:05:01 +0300

vim-youcompleteme (0+20160327+git1b76af4-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches for new upstreal release.
  * Remove 01-vim-expression-to-python-type.patch (fixed in upstream).
  * Update 00-path-to-server-script.patch to avoid using sys.executable
    for python interpreter path.
  * Add ycmd (<< 0+20160327+gitc3e6904.1) into Build-Depends and Depends.

 -- Onur Aslan <onur@onur.im>  Sat, 09 Apr 2016 12:24:46 +0300

vim-youcompleteme (0+20160229+git94ec3ed-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 3.9.7 (no changes).
  * Update ycmd dependency version in d/control.
  * Update 00-path-to-server-script.patch to set ycmd path in ycm.paths
    module.
  * Increase complexity for flake8 in d/rules override_dh_auto_test.
  * Use HTTPS URI's for Vcs-Git and Vcs-Browser fields.
  * Remove python-flake8 and add flake8 into Build-Depends.
  * Add 01-vim-expression-to-python-type.patch.

 -- Onur Aslan <onur@onur.im>  Tue, 29 Mar 2016 14:06:03 +0300

vim-youcompleteme (0+20151025+gitf20c693-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches for new upstream version.
  * Update ycmd version in Build-Depends and Depends.
  * Add python-requests and python-requests-futures into Build-Depends.
  * Add retries to PYTHONPATH for nosetests.

 -- Onur Aslan <onur@onur.im>  Mon, 02 Nov 2015 13:41:28 +0300

vim-youcompleteme (0+20150806+gitc5a5145-1) unstable; urgency=low

  * New upstream release.
  * Add ycmd dependency version requirement.
  * override_dh_auto_test in debian/rules to enable tests.
  * Add python-flake8, python-hamcrest, python-mock, python-nose,
    python-flake8 and ycmd into Build-Depends for tests.
  * Add copyright info for: python/ycm/tests/vimsupport_test.py.
  * Update copyright years.

 -- Onur Aslan <onur@onur.im>  Fri, 07 Aug 2015 12:25:01 +0300

vim-youcompleteme (0+20150616+gitbc5f581-1) unstable; urgency=low

  * New upstream release. (Closes: #758021)
  * ycmd is separated from project and no longer part of this package.
    All patches, Build-Depends and Depends related to ycmd have been removed.
  * Use dpt (debian-perl-tools) repack.sh to repack source in
    debian/rules get-orig-source.
  * Remove ycmd related overrides in debian/rules.
  * Remove examples and ycmd example: ycm_extra_conf.py
  * Update copyright information.
  * Add ycmd dependency.
  * Add python-requests-futures dependency. Remove embedded copy of
    python-requests-futures from source. (Closes: #780292)
  * Install python directory into /u/s/vim-youcompleteme/python/.
  * Package architecture is all now.
  * Update Vcs-Browser URL for cgit.
  * Replace python-dev dependency with python.

 -- Onur Aslan <onur@onur.im>  Mon, 13 Jul 2015 12:31:40 +0300

vim-youcompleteme (0+20140207+git18be5c2-2) unstable; urgency=low

  [ Onur Aslan ]
  * Enable tests for ycm_core
  * Add google-mock and libgtest-dev to Build-Depends
  * More detailed description for 00-build-system.patch
  * Override dh_auto_test to run ycm_core_tests
  * Use canonical URI in Vcs-Git
  * Add minimum version requirement to python-requests dependency
    (Closes: #745768)
  * Bump standards to 3.9.6

  [ Sebastian Ramacher ]
  * debian/patches/03-tempdir.patch: Update patch to fix issues with
    :YcmRestartServer if the temporary directory got deleted to early.
    (Closes: #745496)

 -- Onur Aslan <onur@onur.im>  Mon, 13 Oct 2014 21:19:31 +0300

vim-youcompleteme (0+20140207+git18be5c2-1) unstable; urgency=low

  [ Onur Aslan ]
  * New upstream version
  * Support for llvm-3.4 and llvm-3.5
  * Refresh and update patches for new upstream version:
    + 00-build-system.patch:
      - Remove BoostParts from target_link_libraries for both CLIENT_LIB
        and SERVER_LIB
      - Remove Boost component thread and libboost-thread-dev from
        Build-Depends
      - Use cmake file function to find llvm include directory
      - Use additional clang versions (3.3, 3.4 and 3.5) in find_library
    + 01-script-folder-path.patch:
      - Use only one PYTHONPATH (/usr/lib/vim-youcompleteme) in vim plugin
        and python application
    + 02-default-conf.patch:
      - Set global_ycm_extra_conf in new configuration
  * New python dependencies
  * Install third party libraries into PYTHONPATH
  * Copyright information for third party libraries
  * Remove tests from final package
  * Exclude some third party libraries from orig.tar.gz, they already exist
    in Debian
  * Use minimum python version 2.7
  * Update Vcs-Git and Vcs-Browser fields

  [ Sebastian Ramacher ]
  * debian/patches/03-tempdir.patch: Use temporary directory instead of
    /tmp/ycm_temp so that names of edited files are not leaked to other users.

 -- Onur Aslan <onur@onur.im>  Thu, 13 Feb 2014 23:55:54 +0200

vim-youcompleteme (0+20131009+gitcbb43ba-1) unstable; urgency=low

  * Initial release. (Closes: #700217)

 -- Onur Aslan <onur@onur.im>  Tue, 17 Dec 2013 11:12:14 +0200
